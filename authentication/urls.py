from django.urls import path
from authentication import views

urlpatterns = [
    path('login/', views.login_view, name = 'login'),
    path('sign_up/', views.sign_up_view, name = 'sign_up'),
    path('logout/', views.logout_view, name = 'logout'),
    path('edit_profile/', views.edit_profile, name = 'edit_profile'),
    path('change_password/', views.change_password, name='change_password'),
    path('profile/', views.view_profile, name= 'profile')
]