from django.shortcuts import render, redirect

from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from .forms import UserCreation, EditProfileForm, ChangePasswordForm
from django.contrib.auth.decorators import login_required
from posts.models import Post

def login_view(request):
    context = {
        'hide_nav': True,
        'hide_footer': True  # Скрываем футер на странице входа
    }
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            return redirect('index')
        else:
            context['has_error'] = True
    return render(request, 'login.html', context)

def logout_view(request):
    logout(request)
    return redirect('index')

from django.shortcuts import render, redirect
from .forms import UserCreation
from django.contrib.auth import login

def sign_up_view(request):
    if request.method == 'POST':
        form = UserCreation(data=request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('index')
    else:
        form = UserCreation()

    context = {
        'form': form,
        'hide_nav': True,
        'hide_footer' : True
    }
    return render(request, 'sign_up.html', context=context)

def view_profile(request):
    user_posts = Post.objects.filter(author=request.user)  ## Получение постов текущего пользователя
    return render(request, 'profile.html', {
        'user': request.user,
        'user_posts': user_posts
    })

def edit_profile(request):
    if request.method == 'POST':
        form = EditProfileForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            return redirect('profile')
    else:
        form = EditProfileForm(instance=request.user)
    return render(request, 'edit_profile.html', {'form': form})

def change_password(request):
    if request.method == 'POST':
        form = ChangePasswordForm(data=request.POST, user=request.user)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            return redirect('profile')
    else:
        form = ChangePasswordForm(user=request.user)
    return render(request, 'change_password.html', {'form': form})