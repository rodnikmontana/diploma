from django.shortcuts import render

def index_view(request):
    username = None
    if request.user.is_authenticated:
        username = request.user.username
    context = {'username' : username}
    return render(request, 'index.html', context)