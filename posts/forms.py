from django import forms
from django.forms import widgets

class PostForm(forms.Form):
    title = forms.CharField(max_length=200, required=True, label='Title', widget=widgets.TextInput(attrs={'class': 'form-control'}))
    body = forms.CharField(max_length=3000, required=False, label='Body', widget=widgets.Textarea(attrs={'class': 'form-control', 'rows': 4}))
    image = forms.ImageField(required=False, label='Image', widget=widgets.FileInput(attrs={'class': 'form-control'}))