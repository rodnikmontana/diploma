from django.db import models
from django.contrib.auth.models import User

class Post(models.Model):
    title = models.CharField(max_length=100, null=False, blank=False, verbose_name='Title')
    image = models.ImageField(upload_to='post_images/', blank=True, null=True)
    body = models.TextField(max_length=5000, null = True, blank=True, verbose_name='Body')
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add = True, verbose_name = 'Creation date')
    updated_at = models.DateTimeField(auto_now = True, verbose_name = 'Edit date')

    def __str__(self):
        return f'{self.pk} - {self.title}'