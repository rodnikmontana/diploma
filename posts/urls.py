from django.urls import path
from posts import views

urlpatterns = [
    path('post_create/', views.post_create_view, name = 'post_create'),
    path('post_delete/<int:pk>/', views.post_delete_view, name = 'post_delete'),
    path('post_update/<int:pk>/', views.post_update_view, name = 'post_update'),
    path('post_view/post_<int:pk>/', views.post_view, name = 'post_view'),
    path('posts_list/', views.posts_list_view, name = 'posts_list')
]