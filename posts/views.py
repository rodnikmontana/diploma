from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required

from .models import Post
from .forms import PostForm

## CRUD operations

## Create:
@login_required
def post_create_view(request):
    if request.method == 'GET':
        form = PostForm()
        return render(request, 'post_create.html', {'form': form})
    elif request.method == 'POST':
        form = PostForm(request.POST, request.FILES)
        if form.is_valid():
            title = form.cleaned_data['title']
            body = form.cleaned_data['body']
            image = form.cleaned_data['image']
            new_post = Post(
                title=title,
                body=body,
                author=request.user,
                image=image if image else None
            )
            new_post.save()
            return redirect('post_view', pk=new_post.pk)
        else:
            return render(request, 'post_create.html', {'form': form})

## Read
def posts_list_view(request):
    sort_by = request.GET.get('sort', 'date')  # Получаем параметр сортировки из запроса или используем 'date' по умолчанию

    if sort_by == 'title':
        posts = Post.objects.all().order_by('title')  # Сортировка по заголовку
    else:
        posts = Post.objects.all().order_by('-created_at')  # Сортировка по дате создания (по умолчанию)

    return render(request, 'posts_list.html', context={'posts': posts, 'sort_by': sort_by})

def post_view(request, *args, **kwargs):
    post = get_object_or_404(Post, pk = kwargs.get('pk'))
    return render(request, 'post_view.html',
                  context = {'post' : post})

## Update
def post_update_view(request, pk):
    post = get_object_or_404(Post, pk=pk)

    if request.method == 'GET':
        form = PostForm(initial={
            'title': post.title,
            'body': post.body,
            # Не добавляйте 'image' здесь, так как это форма не модельная
        })
        return render(request, 'post_update.html', {'form': form, 'post': post})
    
    elif request.method == 'POST':
        form = PostForm(request.POST, request.FILES)

        if form.is_valid():
            post.title = form.cleaned_data.get('title')
            post.body = form.cleaned_data.get('body')
            
            if 'delete_image' in request.POST and post.image:
                post.image.delete()  # Удаление изображения
                post.image = None
            elif 'image' in request.FILES:
                post.image = request.FILES['image']

            post.author = request.user  # Проверьте, нужно ли обновлять автора
            post.save()
            return redirect('post_view', pk=post.pk)
        else:
            return render(request, 'post_update.html', {'form': form, 'post': post})
        
## Delete      
def post_delete_view(request, *args, **kwargs):
    post = get_object_or_404(Post, pk = kwargs.get('pk'))
    if request.method == 'GET':
        return render(request, 'post_delete.html', context = {'post' : post})
    
    elif request.method == 'POST':
        post.delete()
        return redirect('posts_list')