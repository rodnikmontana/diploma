from django.urls import path
from . import views

urlpatterns = [
    path('download_ndvi/', views.download_ndvi, name='download_ndvi'),
    path('download_ndmi/', views.download_ndmi, name='download_ndmi'),
    path('download_ndwi/', views.download_ndwi, name='download_ndwi'),
    path('download_ndmi_excel/', views.download_ndmi_excel, name='download_ndmi_excel'),
    path('download_ndwi_excel/', views.download_ndwi_excel, name='download_ndwi_excel'),
    path('download_ndvi_excel/', views.download_ndvi_excel, name='download_ndvi_excel')
]