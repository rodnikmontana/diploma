from django.http import HttpResponse, FileResponse
from django.shortcuts import redirect
from django.contrib import messages


## Скачивание рез-татов в PNG
def download_ndvi(request):
    ## Получаем путь к файлу NDVI из сессии пользователя
    ndvi_result_file_path = request.session.get('ndvi_result_file_path', None)

    if ndvi_result_file_path:
        ## Отправляем файл пользователю
        try:
            with open(ndvi_result_file_path, 'rb') as file:
                response = HttpResponse(file.read(), content_type='image/png')
                response['Content-Disposition'] = 'attachment; filename="ndvi_result.png"'
                return response
        except FileNotFoundError:
            messages.error(request, 'File not found.')
            return redirect('calculate_ndvi')
    else:
        messages.error(request, 'NDVI file not found in session.')
        return redirect('calculate_ndvi')

def download_ndmi(request):
    ndmi_result_file_path = request.session.get('ndmi_result_file_path', None)

    if ndmi_result_file_path:
        try:
            with open(ndmi_result_file_path, 'rb') as file:
                response = HttpResponse(file.read(), content_type='image/png')
                response['Content-Disposition'] = 'attachment; filename="ndmi_result.png"'
                return response
        except FileNotFoundError:
            messages.error(request, 'File not found.')
            return redirect('calculate_ndmi')
    else:
        messages.error(request, 'NDMI file not found in session.')
        return redirect('calculate_ndmi')

def download_ndwi(request):
    ndwi_result_file_path = request.session.get('ndwi_result_file_path', None)

    if ndwi_result_file_path:
        try:
            with open(ndwi_result_file_path, 'rb') as file:
                response = HttpResponse(file.read(), content_type='image/png')
                response['Content-Disposition'] = 'attachment; filename="ndwi_result.png"'
                return response
        except FileNotFoundError:
            messages.error(request, 'File not found.')
            return redirect('calculate_ndwi')
    else:
        messages.error(request, 'NDWI file not found in session.')
        return redirect('calculate_ndwi')

## Скачивание рез-татов в EXCEL

def download_ndwi_excel(request):
    ndwi_excel_file_path = request.session.get('ndwi_excel_file_path', None)

    if ndwi_excel_file_path:
        try:
            with open(ndwi_excel_file_path, 'rb') as file:
                response = HttpResponse(file.read(), content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
                response['Content-Disposition'] = 'attachment; filename="ndwi_results.xlsx"'
                return response
        except FileNotFoundError:
            messages.error(request, 'File not found.')
            return redirect('calculate_ndwi')
    else:
        messages.error(request, 'NDWI Excel file not found in session.')
        return redirect('calculate_ndwi')

def download_ndvi_excel(request):
    ndvi_excel_file_path = request.session.get('ndvi_excel_file_path', None)

    if ndvi_excel_file_path:
        try:
            with open(ndvi_excel_file_path, 'rb') as file:
                response = HttpResponse(file.read(), content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
                response['Content-Disposition'] = 'attachment; filename="ndvi_results.xlsx"'
                return response
        except FileNotFoundError:
            messages.error(request, 'File not found.')
            return redirect('calculate_ndvi')
    else:
        messages.error(request, 'NDVI Excel file not found in session.')
        return redirect('calculate_ndvi')

def download_ndmi_excel(request):
    ndmi_excel_file_path = request.session.get('ndmi_excel_file_path', None)

    if ndmi_excel_file_path:
        try:
            with open(ndmi_excel_file_path, 'rb') as file:
                response = HttpResponse(file.read(), content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
                response['Content-Disposition'] = 'attachment; filename="ndmi_results.xlsx"'
                return response
        except FileNotFoundError:
            messages.error(request, 'File not found.')
            return redirect('calculate_ndmi')
    else:
        messages.error(request, 'NDMI Excel file not found in session.')
        return redirect('calculate_ndmi')
