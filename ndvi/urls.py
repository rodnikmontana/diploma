from django.urls import path
from ndvi import views

urlpatterns = [
    path('calculate_ndvi/', views.ndvi, name='calculate_ndvi')
]