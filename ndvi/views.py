from django.shortcuts import render, redirect
from .forms import NDVIForm
from django.http import HttpResponse
from django.contrib import messages
import time
import os
import rasterio as rio
import numpy as np
import matplotlib.pyplot as plt
from rasterio.plot import reshape_as_image
import pandas as pd

def ndvi(request):
    form = NDVIForm()
    if request.method == 'POST':
        form = NDVIForm(request.POST, request.FILES)
        if form.is_valid():
            red_band = request.FILES['red_band']
            nir_band = request.FILES['nir_band']
            
            temp_folder = 'temp/ndvi'
            red_band_path = os.path.join(temp_folder, 'red_band.tiff')
            nir_band_path = os.path.join(temp_folder, 'nir_band.tiff')

            with open(red_band_path, 'wb+') as destination:
                for chunk in red_band.chunks():
                    destination.write(chunk)

            with open(nir_band_path, 'wb+') as destination:
                for chunk in nir_band.chunks():
                    destination.write(chunk)

            with rio.open(red_band_path) as src_red, rio.open(nir_band_path) as src_nir:
                red = reshape_as_image(src_red.read())
                nir = reshape_as_image(src_nir.read())
                
                ndvi = np.nan_to_num((nir - red) / (nir + red))

                ndvi_mean = np.mean(ndvi)

            plt.figure(figsize=[10, 10])
            plt.imshow(ndvi, cmap='RdYlGn', vmin=-1, vmax=1)
            plt.colorbar(label='NDVI')
            plt.title('Normalized Difference Vegetation Index (NDVI)')
            plt.axis('off')
            plt.tight_layout()
            
            timestamp = str(int(time.time()))

            plt.savefig('ndvi/static/images/ndvi_image_' + timestamp + '.png', bbox_inches='tight', pad_inches=0)
            plt.close()

            ndvi_data = {'NDVI': [ndvi_mean]}
            ndvi_df = pd.DataFrame(ndvi_data)
            excel_file_path = 'ndvi/excel/ndvi_results_' + timestamp + '.xlsx'
            ndvi_df.to_excel(excel_file_path, index=False)

            request.session['ndvi_result_file_path'] = 'ndvi/static/images/ndvi_image_' + timestamp + '.png'
            request.session['ndvi_excel_file_path'] = excel_file_path

            os.remove(red_band_path)
            os.remove(nir_band_path)

            return render(request, 'ndvi_result.html', {'ndvi_image': True, 'image_name': 'ndvi_image_' + timestamp + '.png'})

    return render(request, 'ndvi_calculation.html', {'form': form, 'error_message': 'Please upload a TIFF file.'})