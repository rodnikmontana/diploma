from django import forms

class NDVIForm(forms.Form):
    red_band = forms.ImageField(label='Upload Red Band Image')
    nir_band = forms.ImageField(label='Upload Near-Infrared Band Image')
