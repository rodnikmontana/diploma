from django.urls import path
from ndmi import views

urlpatterns = [
    path('calculate_ndmi/', views.ndmi, name='calculate_ndmi')
]