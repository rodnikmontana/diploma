# ndmi/views.py
from django.shortcuts import render, redirect
from .forms import NDMIForm
from django.http import HttpResponse
from django.contrib import messages
import time
import os
import rasterio as rio
import numpy as np
import matplotlib.pyplot as plt
from rasterio.plot import reshape_as_image
import pandas as pd

def ndmi(request):
    form = NDMIForm()
    if request.method == 'POST':
        form = NDMIForm(request.POST, request.FILES)
        if form.is_valid():
            nir_band = request.FILES['nir_band']
            swir_band = request.FILES['swir_band']

            temp_folder = 'temp/ndmi'
            nir_band_path = os.path.join(temp_folder, 'nir_band.tiff')
            swir_band_path = os.path.join(temp_folder, 'swir_band.tiff')

            with open(nir_band_path, 'wb+') as destination:
                for chunk in nir_band.chunks():
                    destination.write(chunk)
            
            with open(swir_band_path, 'wb+') as destination:
                for chunk in swir_band.chunks():
                    destination.write(chunk)
            
            with rio.open(nir_band_path) as src_nir, rio.open(swir_band_path) as src_swir:
                nir = reshape_as_image(src_nir.read())
                swir = reshape_as_image(src_swir.read())

                ndmi = np.nan_to_num((nir - swir) / (nir + swir))
                ndmi_mean = np.mean(ndmi)

            plt.figure(figsize=[10, 10])
            plt.imshow(ndmi, cmap='RdBu', vmin=-1, vmax=1)
            plt.colorbar(label='NDMI')
            plt.title('Normalized Difference Moisture Index (NDMI)')
            plt.axis('off')
            plt.tight_layout()

            timestamp = str(int(time.time()))

            plt.savefig('ndmi/static/images/ndmi_image_' + timestamp + '.png', bbox_inches='tight', pad_inches=0)
            plt.close()

            ndmi_data = {'NDMI': [ndmi_mean]}
            ndmi_df = pd.DataFrame(ndmi_data)
            excel_file_path = 'ndmi/excel/ndmi_results_' + timestamp + '.xlsx'
            ndmi_df.to_excel(excel_file_path, index=False)

            request.session['ndmi_result_file_path'] = 'ndmi/static/images/ndmi_image_' + timestamp + '.png'
            request.session['ndmi_excel_file_path'] = excel_file_path

            os.remove(nir_band_path)
            os.remove(swir_band_path)

            return render(request, 'ndmi_result.html', {'ndmi_image': True, 'image_name': 'ndmi_image_' + timestamp + '.png'})
        
    return render(request, 'ndmi_calculation.html', {'form': form, 'error_message': 'Please upload a TIFF file.'})