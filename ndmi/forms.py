from django import forms

class NDMIForm(forms.Form):
    nir_band = forms.ImageField(label='Upload Near-Infrared Band Image')
    swir_band = forms.ImageField(label='Upload SWIR Band Image')