from django.apps import AppConfig


class NdmiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ndmi'
