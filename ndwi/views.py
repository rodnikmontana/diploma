# ndwi/views.py
from django.shortcuts import render, redirect
from .forms import NDWIForm
from django.http import HttpResponse
from django.contrib import messages
import time
import os
import rasterio as rio
import numpy as np
import matplotlib.pyplot as plt
from rasterio.plot import reshape_as_image
import pandas as pd

def ndwi(request):
    form = NDWIForm()
    if request.method == 'POST':
        form = NDWIForm(request.POST, request.FILES)
        if form.is_valid():
            green_band = request.FILES['green_band']
            nir_band = request.FILES['nir_band']

            temp_folder = 'temp/ndwi'
            green_band_path = os.path.join(temp_folder, 'green_band.tiff')
            nir_band_path = os.path.join(temp_folder, 'nir_band.tiff')

            with open(green_band_path, 'wb+') as destination:
                for chunk in green_band.chunks():
                    destination.write(chunk)
            
            with open(nir_band_path, 'wb+') as destination:
                for chunk in nir_band.chunks():
                    destination.write(chunk)
            
            with rio.open(green_band_path) as src_green, rio.open(nir_band_path) as src_nir:
                green = reshape_as_image(src_green.read())
                nir = reshape_as_image(src_nir.read())

                ndwi = np.nan_to_num((green - nir) / (green + nir))
                ndwi_mean = np.mean(ndwi)

            plt.figure(figsize=[10, 10])
            plt.imshow(ndwi, cmap='Blues', vmin=-1, vmax=1)
            plt.colorbar(label='NDWI')
            plt.title('Normalized Difference Water Index (NDWI)')
            plt.axis('off')
            plt.tight_layout()

            timestamp = str(int(time.time()))

            plt.savefig('ndwi/static/images/ndwi_image_' + timestamp + '.png', bbox_inches='tight', pad_inches=0)
            plt.close()

            ndwi_data = {'NDWI': [ndwi_mean]}
            ndwi_df = pd.DataFrame(ndwi_data)
            excel_file_path = 'ndwi/excel/ndwi_results_' + timestamp + '.xlsx'
            ndwi_df.to_excel(excel_file_path, index=False)

            request.session['ndwi_result_file_path'] = 'ndwi/static/images/ndwi_image_' + timestamp + '.png'
            request.session['ndwi_excel_file_path'] = excel_file_path

            os.remove(green_band_path)
            os.remove(nir_band_path)

            return render(request, 'ndwi_result.html', {'ndwi_image': True, 'image_name': 'ndwi_image_' + timestamp + '.png'})
        
    return render(request, 'ndwi_calculation.html', {'form': form, 'error_message': 'Please upload a TIFF file.'})
