from django import forms

class NDWIForm(forms.Form):
    green_band = forms.ImageField(label = 'Upload Green Channel Image')
    nir_band = forms.ImageField(label='Upload Near-Infrared Band Image')