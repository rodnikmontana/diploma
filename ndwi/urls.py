from django.urls import path
from ndwi import views

urlpatterns = [
    path('calculate_ndwi/', views.ndwi, name='calculate_ndwi')
]